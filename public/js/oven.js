	var ovenTimer = {
			init: function () {
				this.updateTimer();
				var elm = document.getElementById("nob3");
				elm.addEventListener("click", this.toggleHours.bind(this));
				
				var elm2 = document.getElementById("nob4");
				elm2.addEventListener("click", this.toggleGender.bind(this));
				
				window.setTimeout(function() {document.body.setAttribute("data-loaded", "true");}, 500)
			},
			toggleGender: function() {
				if ( document.body.hasAttribute("data-gender"))
				{
					document.body.removeAttribute("data-gender");
				} else {
					document.body.setAttribute("data-gender", "XY");
				}
			},
			toggleHours: function () {
				this.showHours = !this.showHours;
				document.body.setAttribute("data-show-hours", this.showHours ? "true" : "false");
				this.updateTimer(true);
			},
			showHours: false,
			updateTimer: function (skipNextTimer) {

				var elm = document.getElementById("timer");

				var destDate = Date.parse(elm.getAttribute("data-target-date"))
				var nowDate = new Date();
				var msUntil = (destDate - nowDate);
				var secUntil = parseInt(msUntil / 1000);
				var secs = secUntil % 60
				var minUntil = parseInt(secUntil / 60);
				var mins = minUntil % 60;
				var hourUntil = parseInt(minUntil / 60);

				var dayUntil = parseInt(hourUntil / 24);

				var days = parseInt(dayUntil)
				var hours = dayUntil % 24;

				days = ("___0" + days).substr(-3);
				hours = ("___0" + hours).substr(-2);
				mins = ("___0" + mins).substr(-2);
				secs = ("___0" + secs).substr(-2);


				if (this.showHours) {
					elm.textContent = hourUntil + ":" + mins + ":" + secs;

				} else {
					elm.textContent = days + " " + hours + ":" + mins + ":" + secs;

				}
				if (skipNextTimer === undefined || !skipNextTimer) {
					window.setTimeout(this.updateTimer.bind(this), 1000);
				}


			}
		}

		ovenTimer.init();